import * as ReactDOM from 'react-dom';
import React from 'react'; 
import { runApp, isTestEnvironment } from './index';
import server from './services/server';
import reportWebVitals from './reportWebVitals';
import { store } from './app/store';

jest.mock('./services/server', () => jest.fn());
jest.mock('./reportWebVitals', () => jest.fn());

const mockRender = jest.fn();
jest.mock('react-dom', () => ({
  createRoot: jest.fn(() => ({
    render: mockRender,
  })),
}));

describe('Application Root', () => {
  let originalNodeEnv;

  beforeAll(() => {
    originalNodeEnv = process.env.NODE_ENV;
  });

  afterAll(() => {
    process.env.NODE_ENV = originalNodeEnv; 
  });

  beforeEach(() => {
    jest.clearAllMocks();
  });


  it('should verify if it is the test environment', () => {
    process.env.NODE_ENV = 'production';
    expect(isTestEnvironment()).toBeFalsy();

    process.env.NODE_ENV = 'test';
    expect(isTestEnvironment()).toBeTruthy();
  });

  it('executes runApp in non-test environment', () => {
    process.env.NODE_ENV = 'development';
    jest.isolateModules(() => {
      require('./index');
    });

    expect(runApp).toHaveBeenCalledTimes(1);
  });

  it('does not run in the test environment', () => {
    process.env.NODE_ENV = 'test';

    jest.isolateModules(() => {
      require('./index');
    });

    expect(server).not.toHaveBeenCalled();
    expect(reportWebVitals).not.toHaveBeenCalled();
    expect(ReactDOM.createRoot).not.toHaveBeenCalled();
  });

  it('simulates a full run in a production-like environment', () => {
    process.env.NODE_ENV = 'production';
    const realUseEffect = React.useEffect;
    jest.spyOn(React, 'useEffect').mockImplementation((f) => f()); 
    
    jest.unmock('./services/server');
    const realServer = require('./services/server').default;

    jest.unmock('./reportWebVitals');
    const realReportWebVitals = require('./reportWebVitals').default;

    runApp(); 

    expect(realServer).toHaveBeenCalled(); 
    expect(realReportWebVitals).toHaveBeenCalled(); 

    process.env.NODE_ENV = 'test'; 
    React.useEffect = realUseEffect; 
  });
});