import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { BrowserRouter as Router } from 'react-router-dom';
import App from './App';

jest.mock('./components/Navigation/Navigation', () => {
  return function DummyNavigation() {
    return <div>Navigation</div>;
  };
});

const renderWithRouter = (ui) => {
  return render(<Router>{ui}</Router>);
};

describe('App component', () => {
  it('renders App component', () => {
    renderWithRouter(<App />);
    expect(screen.getByTestId('app')).toBeInTheDocument();
  });

  it('should handle click events for showing and hiding the left section', async () => {
    const { container } = renderWithRouter(<App />);
    const svgContainer = container.querySelector('.click-svg-container');
    
    let leftSections = container.querySelectorAll('.App-left-section');

    expect(leftSections.length).toBe(2); 

    fireEvent.click(svgContainer);

    await waitFor(() => {
      leftSections = container.querySelectorAll('.App-left-section');
      expect(leftSections.length).toBe(0); 
    });

    fireEvent.click(svgContainer);

    await waitFor(() => {
      leftSections = container.querySelectorAll('.App-left-section');
      expect(leftSections.length).toBe(2); 
    });
  });

  it('should render the "Info" component on the default route', () => {
    renderWithRouter(<App />);

    expect(screen.getByText('Your Info Components Text Here')).toBeInTheDocument();
  });
});