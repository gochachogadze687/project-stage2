import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import SkillForm from './SkillForm';
import SkillList from './SkillList';
import { addSkill } from './skillsSlice';
import './Skills.scss';
import { FaEdit } from 'react-icons/fa'; 

function Skills() {
    const [isEditing, setIsEditing] = useState(false);
    const skills = useSelector((state) => state.skills);
    const dispatch = useDispatch();

    const handleOpenEdit = () => {
        setIsEditing(!isEditing);
    };

    const handleAddSkill = (name, range) => {
        dispatch(addSkill({ name, range }));
    };

    return (
        <div className="skills-section">
            <div className="skills-header">
                <h2>Skills</h2>
                <button onClick={handleOpenEdit} className="edit-button">
            {isEditing ? 'Close edit' : <><FaEdit /> Open edit</>}
        </button>
            </div>
            {isEditing && <SkillForm onAddSkill={handleAddSkill} />}
            <SkillList skills={skills} />
        </div>
    );
}

export default Skills;