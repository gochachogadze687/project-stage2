import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import skillsReducer, { addSkill } from './skillsSlice';
import Skills from './Skills';

describe('Skills', () => {
    let store;
    const mockSkill = { name: 'React', range: 80 };

    beforeEach(() => {
        store = configureStore({
            reducer: {
                skills: skillsReducer
            }
        });
        store.dispatch = jest.fn();
    });

    it('adds a new skill when the SkillForm is submitted', () => {
        render(<Provider store={store}><Skills /></Provider>);
        const editButton = screen.getByText('Open edit');

        fireEvent.click(editButton);

        fireEvent.change(screen.getByPlaceholderText('Enter skill name'), { target: { value: mockSkill.name } });
        fireEvent.change(screen.getByPlaceholderText('Enter skill range'), { target: { value: mockSkill.range } });

        fireEvent.click(screen.getByText('Add Skill'));

        expect(store.dispatch).toHaveBeenCalledWith(addSkill(mockSkill));
    });
});