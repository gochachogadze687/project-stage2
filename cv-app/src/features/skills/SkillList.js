import React from 'react';
import { useSelector } from 'react-redux';

function SkillList() {
    const skills = useSelector((state) => state.skills);

    return (
        <div className="skills-section1">
            <div className="skill-list">
                {skills.map((skill, index) => (
                    <div key={index} className="skill-item">
                        <div className="skill-bar">
                            <div className="filled" style={{ width: `${skill.range}%` }}>
                                <span className="skill-name">{skill.name}</span>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
            <div className="skills-scale">
                <div className="line"></div>
                <div className="marker beginner-marker"></div>
                <div className="marker proficient-marker"></div>
                <div className="marker expert-marker"></div>
                <div className="marker master-marker"></div>
                <span className="label beginner-label">Beginner</span>
                <span className="label proficient-label">Proficient</span>
                <span className="label expert-label">Expert</span>
                <span className="label master-label">Master</span>
            </div>
        </div>
    );
}

export default SkillList;