import React from 'react';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import skillsReducer from './skillsSlice'; 
import SkillList from './SkillList';

describe('SkillList', () => {
    const mockSkills = [
        { name: 'React', range: 80 },
        { name: 'Redux', range: 70 }
    ];

    const store = configureStore({
        reducer: {
            skills: skillsReducer
        },
        preloadedState: {
            skills: mockSkills
        }
    });

    it('renders skill items based on the state', () => {
        render(<Provider store={store}><SkillList /></Provider>);

        mockSkills.forEach(skill => {
            expect(screen.getByText(skill.name)).toBeInTheDocument();
        });
    });

    it('renders skill bars with correct width', () => {
        render(<Provider store={store}><SkillList /></Provider>);

        mockSkills.forEach(skill => {
            const skillItem = screen.getByText(skill.name).closest('.filled');
            expect(skillItem).toHaveStyle(`width: ${skill.range}%`);
        });
    });

    it('renders the skills scale correctly', () => {
        render(<Provider store={store}><SkillList /></Provider>);

        expect(screen.getByText('Beginner')).toBeInTheDocument();
        expect(screen.getByText('Proficient')).toBeInTheDocument();
        expect(screen.getByText('Expert')).toBeInTheDocument();
        expect(screen.getByText('Master')).toBeInTheDocument();
    });
});