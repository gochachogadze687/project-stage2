import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { addSkill } from './skillsSlice';

function SkillForm() {
    const dispatch = useDispatch();
    const [skillName, setSkillName] = useState('');
    const [skillRange, setSkillRange] = useState('');
    const [error, setError] = useState(''); 

    const isSkillNameValid = skillName.trim() !== "";
    const isSkillRangeValid = skillRange !== '' && !isNaN(skillRange) && skillRange >= 10 && skillRange <= 100;

    const handleSubmit = (e) => {
        e.preventDefault();
        setError('');

        if (!isSkillNameValid) {
            setError("Skill name is a required field");
            return;
        }

        if (isNaN(skillRange)) {
            setError("Skill range must be a 'number' type");
            return;
        } 
        
        if (skillRange < 10) {
            setError("Skill range must be greater than or equal to 10");
            return;
        }
        
        if (skillRange > 100) {
            setError("Skill range must be less than or equal to 100");
            return;
        }

        if (isSkillNameValid && isSkillRangeValid) {
            dispatch(addSkill({ name: skillName, range: parseInt(skillRange, 10) }));
            setSkillName('');
            setSkillRange('');
        }
    };

    return (
        <div className="skill-form">
            <label>
                Skill name
                <input 
                    type="text" 
                    className="input-field"
                    placeholder="Enter skill name" 
                    value={skillName} 
                    onChange={(e) => setSkillName(e.target.value)} 
                />
                {!isSkillNameValid && <p className="error-message">Skill name is a required field</p>}
            </label>
            <br/>
            <label>
                Skill range
                <input 
                    type="text" 
                    className="input-field"
                    min="10" 
                    max="100" 
                    placeholder="Enter skill range"
                    value={skillRange} 
                    onChange={(e) => setSkillRange(e.target.value)} 
                />
                {isNaN(skillRange) && <p className="error-message">Skill range must be a 'number' type</p>}
                {skillRange < 10 && skillRange !== '' && <p className="error-message">Skill range must be greater than or equal to 10</p>}
                {skillRange > 100 && <p className="error-message">Skill range must be less than or equal to 100</p>}
            </label>
            <br/>
            <button 
                onClick={handleSubmit} 
                className={`add-button ${isSkillNameValid && isSkillRangeValid ? 'active' : 'inactive'}`}
                disabled={!(isSkillNameValid && isSkillRangeValid)}
            >
                Add Skill
            </button>
        </div>
    );
}

export default SkillForm;