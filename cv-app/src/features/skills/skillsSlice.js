import { createSlice } from '@reduxjs/toolkit';

export const skillsSlice = createSlice({
  name: 'skills',
  initialState: [],
  reducers: {
    addSkill: (state, action) => {
      state.push(action.payload);
    },
  },
});

export const { addSkill } = skillsSlice.actions;
export default skillsSlice.reducer;