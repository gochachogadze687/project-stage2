import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import skillsReducer from './skillsSlice'; 
import SkillForm from './SkillForm';

const store = configureStore({
  reducer: {
    skills: skillsReducer
  }
});

describe('SkillForm', () => {
  it('renders the form fields', () => {
    render(<Provider store={store}><SkillForm /></Provider>);

    expect(screen.getByPlaceholderText("Enter skill name")).toBeInTheDocument();
    expect(screen.getByPlaceholderText("Enter skill range")).toBeInTheDocument();
  });

  it('updates the skill name and skill range on user input', () => {
    render(<Provider store={store}><SkillForm /></Provider>);

    fireEvent.change(screen.getByPlaceholderText("Enter skill name"), { target: { value: 'React' } });
    fireEvent.change(screen.getByPlaceholderText("Enter skill range"), { target: { value: '90' } });

    expect(screen.getByPlaceholderText("Enter skill name").value).toBe('React');
    expect(screen.getByPlaceholderText("Enter skill range").value).toBe('90');
  });

  it('shows error messages for invalid inputs', () => {
    render(<Provider store={store}><SkillForm /></Provider>);

    fireEvent.change(screen.getByPlaceholderText("Enter skill name"), { target: { value: '' } });
    fireEvent.change(screen.getByPlaceholderText("Enter skill range"), { target: { value: '5' } });

    expect(screen.getByText("Skill name is a required field")).toBeInTheDocument();
    expect(screen.getByText("Skill range must be greater than or equal to 10")).toBeInTheDocument();
  });

  it('dispatches the addSkill action on valid form submission', () => {
    const mockDispatch = jest.fn();
    jest.mock('react-redux', () => ({
      ...jest.requireActual('react-redux'),
      useDispatch: () => mockDispatch
    }));

    render(<Provider store={store}><SkillForm /></Provider>);

    fireEvent.change(screen.getByPlaceholderText("Enter skill name"), { target: { value: 'React' } });
    fireEvent.change(screen.getByPlaceholderText("Enter skill range"), { target: { value: '90' } });
    fireEvent.click(screen.getByText('Add Skill'));

    expect(mockDispatch).toHaveBeenCalledWith(addSkill({ name: 'React', range: 90 }));
  });
});