import { createSlice } from '@reduxjs/toolkit';

export const educationSlice = createSlice({
  name: 'education',
  initialState: [],
  reducers: {
  },
});

export default educationSlice.reducer;