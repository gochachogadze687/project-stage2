import educationReducer from './educationSlice';

describe('education reducer', () => {
  it('should handle initial state', () => {
    expect(educationReducer(undefined, {})).toEqual([]);
  });
});
