import React from 'react';
import './Box.scss';
import { title as defaultTitle, content as defaultContent } from './BoxText';

function Box({ title = defaultTitle, content = defaultContent }) {
    return (
        <div className="boxWrapper">
            <h2 className="boxTitle">{title}</h2>
            <p className="boxContent">{content}</p>
        </div>
    );
}

export default Box;