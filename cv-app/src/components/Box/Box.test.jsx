import React from 'react';
import { render, screen } from '@testing-library/react';
import Box from './Box';
import { title as defaultTitle, content as defaultContent } from './BoxText';

describe('Box component', () => {
  
  it('renders default title and content when no props are passed', () => {
    render(<Box />);
    const titleElement = screen.getByText(new RegExp(defaultTitle, 'i'));
    const contentElement = screen.getByText(new RegExp(defaultContent, 'i'));
    expect(titleElement).toBeInTheDocument();
    expect(contentElement).toBeInTheDocument();
  });

  it('renders passed title and content', () => {
    const customTitle = "Custom Title";
    const customContent = "This is a custom content.";
    render(<Box title={customTitle} content={customContent} />);
    
    const titleElement = screen.getByText(new RegExp(customTitle, 'i'));
    const contentElement = screen.getByText(new RegExp(customContent, 'i'));
    expect(titleElement).toBeInTheDocument();
    expect(contentElement).toBeInTheDocument();
  });
});