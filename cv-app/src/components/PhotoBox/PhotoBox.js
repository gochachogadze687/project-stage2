import React from 'react';
import UserAvatar from '../../assets/images/UserAvatar.png';
import './PhotoBox.scss';

function PhotoBox({ name, avatar }) {
  return (
    <div className="photo-box">
      <img className="avatar" src={avatar || UserAvatar} alt={'JohnDoe'} />
      <h2 className="name">{'John Doe'}</h2>
    </div>
  );
}

export default PhotoBox;