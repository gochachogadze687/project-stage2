import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { MemoryRouter, Routes, Route } from 'react-router-dom';
import Button from './Button';

describe('Button component', () => {
    test('renders button with text', () => {
        render(
            <MemoryRouter>
                <Button text="Test Button" />
            </MemoryRouter>
        );
        const buttonText = screen.getByText(/Test Button/i);
        expect(buttonText).toBeInTheDocument();
    });

    test('renders button with icon', () => {
        render(
            <MemoryRouter>
                <Button icon={<span>Icon</span>} text="Test Button" />
            </MemoryRouter>
        );
        const iconElement = screen.getByText(/Icon/i);
        expect(iconElement).toBeInTheDocument();
    });

    test('navigates when navigateTo prop is provided', () => {
        render(
            <MemoryRouter initialEntries={["/"]}>
                <Routes>
                    <Route path="/" element={
                        <Button text="Navigate" navigateTo="/target" />
                    }/>
                    <Route path="/target" element={<div>Target Page</div>}/>
                </Routes>
            </MemoryRouter>
        );

        fireEvent.click(screen.getByText(/Navigate/i));
        expect(screen.getByText("Target Page")).toBeInTheDocument();
    });
});