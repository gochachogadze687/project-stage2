import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import ScrollToTop from './ScrollToTop';

global.scrollTo = jest.fn();

describe('ScrollToTop component', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('renders without crashing', () => {
    render(<ScrollToTop />);
    const buttonElement = screen.getByText('∧');
    expect(buttonElement).toBeInTheDocument();
  });

  it('displays the scroll to top button', () => {
    render(<ScrollToTop />);
    expect(screen.getByText('∧')).toBeInTheDocument();
  });

  it('calls scrollTo when button is clicked', () => {
    render(<ScrollToTop />);
    const button = screen.getByText('∧');
    fireEvent.click(button);
    expect(global.scrollTo).toHaveBeenCalledWith({
      top: 0,
      behavior: 'smooth'
    });
  });
});