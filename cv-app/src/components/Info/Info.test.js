import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { BrowserRouter as Router } from 'react-router-dom';
import Info from './Info';

const renderWithRouter = (ui) => {
  return render(<Router>{ui}</Router>);
};

describe('Info component', () => {
  beforeEach(() => {
    renderWithRouter(<Info />);
  });

  it('renders with a background image', () => {
    const infoContainer = screen.getByRole('img', { name: /background/i }); 
    expect(infoContainer).toHaveStyle({
      backgroundImage: expect.stringContaining('background.png'), 
    });
  });

  it('renders the user avatar', () => {
    const userImage = screen.getByRole('img', { name: /john doe/i }); 
    expect(userImage).toBeInTheDocument();
    expect(userImage).toHaveAttribute('src', expect.stringContaining('UserAvatar.png')); 
  });

  it('renders the title and texts correctly', () => {
    const title = screen.getByRole('heading', { name: /john doe/i });
    expect(title).toBeInTheDocument();

    const description = screen.getByText(/Lorem ipsum dolor sit amet, consectetuer adipiscing elit./i);
    expect(description).toBeInTheDocument();

  });

  it('renders a link to the "inner" page with correct text', () => {
    const knowMoreLink = screen.getByRole('link', { name: /know more/i });
    expect(knowMoreLink).toBeInTheDocument();
    expect(knowMoreLink).toHaveAttribute('href', '/inner');
  });

});