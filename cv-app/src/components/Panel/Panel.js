import React, { useState } from 'react';
import Navigation from '../Navigation/Navigation';
import Button from '../Button/Button';
import PhotoBox from '../PhotoBox/PhotoBox';

function Panel() {
  const [isOpen, setIsOpen] = useState(false);

  const togglePanel = () => {
    setIsOpen(!isOpen);
  }

  return (
    <div className={`panel ${isOpen ? 'open' : ''}`}>
      <Button onClick={togglePanel} icon="hamburger" />
      <PhotoBox />
      <Navigation />
    </div>
  );
}

export default Panel;