import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';
import Panel from './Panel';

describe('Panel Component', () => {
  let originalUseState;
  let useStateMock;

  beforeEach(() => {
    useStateMock = jest.fn();
    originalUseState = React.useState;
    React.useState = () => [useStateMock.mock.calls.length - 1, useStateMock];
  });

  afterEach(() => {
    React.useState = originalUseState;
  });

  it('should render correctly', () => {
    const { asFragment } = render(<Panel />);
    expect(asFragment()).toMatchSnapshot();
  });

  it('should have a closed panel initially', () => {
    render(<Panel />);
    expect(useStateMock).toHaveBeenCalledWith(false); 
  });

  it('should toggle panel state when button is clicked', () => {
    render(<Panel />);
    const button = screen.getByRole('button', { name: /hamburger icon button/i }); 

    fireEvent.click(button);

    expect(useStateMock).toHaveBeenCalledTimes(1); 
  });

});