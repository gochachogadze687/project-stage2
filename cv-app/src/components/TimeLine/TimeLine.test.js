import React from 'react';
import { render, screen, act } from '@testing-library/react';
import TimeLine from './TimeLine';

describe('TimeLine component', () => {

  jest.useFakeTimers();

  afterEach(() => {
    jest.clearAllMocks();
    jest.clearAllTimers();
  });

  it('renders without crashing', () => {
    render(<TimeLine />);
    expect(screen.getByRole('list')).toBeInTheDocument();
    expect(screen.getByText('Education')).toBeInTheDocument();
  });

  it('displays loading spinner while fetching data', () => {
    render(<TimeLine />);
    expect(screen.getByRole('img', { name: /sync-alt icon/i })).toBeInTheDocument();
  });

  it('displays error message on data fetch error', async () => {
    Math.random = jest.fn().mockReturnValue(0.1); 
    render(<TimeLine />);
    
    act(() => {
      jest.advanceTimersByTime(2000);
    });

    expect(screen.getByText('Something went wrong; please review your server connection!')).toBeInTheDocument();
  });

  it('renders timeline events on successful data fetch', async () => {
    Math.random = jest.fn().mockReturnValue(0.9); 
    render(<TimeLine />);
    
    act(() => {
      jest.advanceTimersByTime(2000);
    });

  });

});