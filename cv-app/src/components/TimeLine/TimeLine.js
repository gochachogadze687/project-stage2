import React, { useState, useEffect } from 'react';
import TimeLineData from './TimeLineData.js';
import './TimeLine.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'; 
import { faSyncAlt } from '@fortawesome/free-solid-svg-icons'; 

function TimeLine() {

    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);

    useEffect(() => {
        setTimeout(() => {
            if (Math.random() < 0.2) { 
                setError('Something went wrong; please review your server connection!');
                setLoading(false);
            } else {
                setLoading(false);
            }
        }, 2000);
    }, []);

    return (
        <div className="timeline-container" role="list">
            <h1 className='Title'>Education</h1>

            {loading ? (
                <div className="loading-container">
                    <FontAwesomeIcon className="icon" icon={faSyncAlt} spin />
                </div>
            ) : error ? (
                <div className="error-message">
                    {error}
                </div>
            ) : (
                TimeLineData.map((event, index, array) => (
                    <div key={index} className="timeline-event" role="listitem">
                        <div className="dates-container">
                            <div className="event-date">{event.date}</div>
                            {index !== array.length - 1 && <div className="vertical-line"></div>}
                        </div>
                        <div className="content-container">
                            <h3>{event.title}</h3>
                            <p className="text">{event.text}</p>
                        </div>
                    </div>
                ))
            )}
        </div>
    );
}

export default TimeLine;