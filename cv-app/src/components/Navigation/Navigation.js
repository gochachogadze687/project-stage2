import React, { useState } from 'react';
import './Navigation.css';
import { Link } from 'react-scroll';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faGraduationCap,
  faPencilAlt,
  faSuitcase,
  faLocationArrow,
  faComment,
  faUser
} from '@fortawesome/free-solid-svg-icons';


const Navigation = () => {
  const [activeItem, setActiveItem] = useState(''); 

  const navItems = [
    { label: 'About me',
      linkTo: 'AboutMe',
      icon: <FontAwesomeIcon icon={faUser} size="lg"/> },

    { label: '  Education', 
      linkTo: 'Education',
      icon: <FontAwesomeIcon icon={faGraduationCap} size="lg" />},

    { label: 'Experience', 
      linkTo: 'Experience',
       icon: <FontAwesomeIcon icon={faPencilAlt} size="lg" />},

    { label: 'Portfolio',
      linkTo: 'Portfolio',
       icon: <FontAwesomeIcon icon={faSuitcase} size="lg" /> },

    { label: 'Contacts', 
      linkTo: 'Contacts',
       icon: <FontAwesomeIcon icon={faLocationArrow} size="lg" />  },

    { label: 'Feedback',
      linkTo: 'Feedback',
       icon: <FontAwesomeIcon icon={faComment} size="lg"  />  },
  ];

  const handleSetActive = (to) => {
    setActiveItem(to);
  };

  return (
    <nav>
      <ul className="nav-list">
        {navItems.map((item, index) => (
          <li key={index} className={`nav-item ${activeItem === item.linkTo ? 'active' : ''}`}>
            <Link
              activeClass="active"
              to={item.linkTo}
              spy={true}
              smooth={true}
              offset={-70}
              duration={500}
              onSetActive={() => handleSetActive(item.linkTo)}
            >
              {item.icon}
              <span className="nav-label">{item.label}</span>
            </Link>
          </li>
        ))}
      </ul>
    </nav>
  );
};
export default Navigation;