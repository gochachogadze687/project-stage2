import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import Navigation from './Navigation';

describe('Navigation component', () => {
  it('renders the correct number of navigation items', () => {
    render(<Navigation />);
    const navItems = screen.getAllByRole('listitem');
    expect(navItems.length).toBe(6);
  });

  it('renders each item with the expected label', () => {
    render(<Navigation />);
    expect(screen.getByText('About me')).toBeInTheDocument();
    expect(screen.getByText('Education')).toBeInTheDocument();
    expect(screen.getByText('Experience')).toBeInTheDocument();
    expect(screen.getByText('Portfolio')).toBeInTheDocument();
    expect(screen.getByText('Contacts')).toBeInTheDocument();
    expect(screen.getByText('Feedback')).toBeInTheDocument();
  });

  it('sets the active class for an active item', () => {
    render(<Navigation />);
    const navItem = screen.getByText('About me').closest('li');
    act(() => {
      fireEvent.click(navItem);
    });
    expect(navItem).toHaveClass('active');
  });
});