import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import Portfolio from './Portfolio';

describe('Portfolio component', () => {
  it('renders without crashing', () => {
    render(<Portfolio />);
    const portfolioElement = screen.getByText('Portfolio');
    expect(portfolioElement).toBeInTheDocument();
  });

  it('displays the title "Portfolio"', () => {
    render(<Portfolio />);
    expect(screen.getByText('Portfolio')).toBeInTheDocument();
  });

  it('has filter buttons for All, UI, and Code', () => {
    render(<Portfolio />);
    expect(screen.getByText('All')).toBeInTheDocument();
    expect(screen.getByText('UI')).toBeInTheDocument();
    expect(screen.getByText('Code')).toBeInTheDocument();
  });

  it('filters items correctly for UI', () => {
    render(<Portfolio />);
    const uiButton = screen.getByText('UI');
    fireEvent.click(uiButton);

    const items = screen.getAllByRole('img');
    expect(items).toHaveLength(2);
  });

  it('filters items correctly for Code', () => {
    render(<Portfolio />);
    const codeButton = screen.getByText('Code');
    fireEvent.click(codeButton);

    const items = screen.getAllByRole('img');
    expect(items).toHaveLength(1);
  });

  it('shows all items when All is clicked', () => {
    render(<Portfolio />);
    const allButton = screen.getByText('All');
    fireEvent.click(allButton);

    const items = screen.getAllByRole('img');
    expect(items).toHaveLength(3);
  });

});