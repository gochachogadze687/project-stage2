import React, { useState } from 'react';
import './Portfolio.scss'
import card1Image from '../../assets/images/card_1.png';
import card2Image from '../../assets/images/card_2.png';
import card3Image from '../../assets/images/card_3.png';

function Portfolio() {
  const [filter, setFilter] = useState('*');

  const handleFilterClick = (selectedFilter) => {
    setFilter(selectedFilter);
  }

  const items = [
    { type: 'ui', img: card1Image, alt: "Item 1" },
    { type: 'code', img: card2Image, alt: "Item 2" },
    { type: 'ui', img: card3Image, alt: "Item 3" }
  ];

  const filteredItems = items.filter(item => filter === '*' || item.type === filter);

  return (
    <div className='portfolioDiv'>
      <h3 className='PortfolioTitle'>Portfolio</h3>
      
      <div className="portfolio-filters">
        <button onClick={() => handleFilterClick('*')}>All</button>
        <button onClick={() => handleFilterClick('ui')}>UI</button>
        <button onClick={() => handleFilterClick('code')}>Code</button>
      </div>

      <div className="grid"
        data-isotope='{ "itemSelector": ".grid-item", "masonry": { "columnWidth": 200 } }'>
        {filteredItems.map(item => (
          <div key={item.alt} className={`grid-item ${item.type}`}>
            <img src={item.img} alt={item.alt} className='PortfolioImage'/>
            <div className="text-container">
              <h3 className='titleText'>Some text</h3>
              <p className='describeText'>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis </p>
              <a href="#" className='linkText'>View resource</a>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default Portfolio;