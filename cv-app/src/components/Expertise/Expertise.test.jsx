import React from 'react';
import { render, screen } from '@testing-library/react';
import Expertise from './Expertise';

describe('Expertise component', () => {

  it('renders correctly when no props are passed', () => {
    render(<Expertise />);

    const defaultElement = screen.queryByText(/default text/i); 
    
    expect(defaultElement).toBeInTheDocument(); 
  });


  it('renders passed data', () => {
    const customData = [
      {
        date: '2020',
        info: {
          company: 'Custom Company',
          job: 'Custom Job',
          description: 'This is a custom description.'
        }
      }
    ];

    render(<Expertise data={customData} />);
    
    const companyElement = screen.getByText(/Custom Company/i);
    const jobElement = screen.getByText(/Custom Job/i);
    const descriptionElement = screen.getByText(/This is a custom description./i);
    const dateElement = screen.getByText(/2020/i);
    
    expect(companyElement).toBeInTheDocument();
    expect(jobElement).toBeInTheDocument();
    expect(descriptionElement).toBeInTheDocument();
    expect(dateElement).toBeInTheDocument();
  });
});