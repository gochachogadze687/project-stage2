import React from 'react';
import { render, screen } from '@testing-library/react';
import FeedbackList from './FeedbackList';

jest.mock('./FeedbackItem', () => ({ feedback, reporter }) => (
  <div>
    Mocked feedback item - {feedback} by {reporter.name}
  </div>
));

describe('FeedbackList component', () => {
  it('shows message when no feedback is available', () => {
    const originalData = [...FeedbackList.defaultProps.feedbackData]; 
    FeedbackList.defaultProps.feedbackData = [];

    render(<FeedbackList />);
    expect(screen.getByText('No feedback available.')).toBeInTheDocument();

    FeedbackList.defaultProps.feedbackData = originalData; 
  });

  it('renders feedback list title', () => {
    render(<FeedbackList />);
    const titleElement = screen.getByText(/Feedbacks/i);
    expect(titleElement).toBeInTheDocument();
  });

  it('renders feedback items correctly', () => {
    render(<FeedbackList />);
    const feedbackElements = screen.getAllByText(/Mocked feedback item/i);
    expect(feedbackElements.length).toBe(2); 
  });

  it('matches the snapshot', () => {
    const { asFragment } = render(<FeedbackList />);
    expect(asFragment()).toMatchSnapshot();
  });
});