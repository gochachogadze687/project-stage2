import React from 'react';
import { render, screen } from '@testing-library/react';
import FeedbackItem from './FeedbackItem';

describe('FeedbackItem component', () => {
  
  it('renders feedback and reporter details correctly', () => {
    const mockData = {
      feedback: 'This is a test feedback.',
      reporter: {
        photoUrl: './mockImage.jpg',
        name: 'John Doe',
        citeUrl: 'https://example.com'
      }
    };
    render(<FeedbackItem {...mockData} />);
    
    const feedbackElement = screen.getByText(/This is a test feedback./i);
    const reporterNameElement = screen.getByText(/John Doe/i);
    const reporterCiteElement = screen.getByText(/somesite.com/i).closest('a');
    
    expect(feedbackElement).toBeInTheDocument();
    expect(reporterNameElement).toBeInTheDocument();
    expect(reporterCiteElement).toHaveAttribute('href', mockData.reporter.citeUrl);
    expect(screen.getByAltText(mockData.reporter.name)).toHaveAttribute('src', mockData.reporter.photoUrl);
  });

  it('renders default props when no props are passed', () => {
    render(<FeedbackItem />);
    
    const feedbackElement = screen.getByText(/No feedback provided./i);
    const reporterNameElement = screen.getByText(/Anonymous/i);
    const reporterCiteElement = screen.getByText(/somesite.com/i).closest('a');
    
    expect(feedbackElement).toBeInTheDocument();
    expect(reporterNameElement).toBeInTheDocument();
    expect(reporterCiteElement).toHaveAttribute('href', '#');
    expect(screen.getByAltText('Anonymous')).toHaveAttribute('src', './default.jpg');
  });
});