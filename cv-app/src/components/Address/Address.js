import React from 'react';
import './Address.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPhone, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { faTwitter, faFacebookF, faSkype } from '@fortawesome/free-brands-svg-icons';

function Address() {
    return (
        <div className="address-component">
        <h1 className='title'>Contacts</h1>
<ul>
    <li>
        <a href="tel:500342242">
            <FontAwesomeIcon icon={faPhone} size="lg" style={{ color: '#26C17E',  marginRight: '20px' }} />
            <span>500 342 242</span>
        </a>
    </li>
    <li>
        <a href="mailto:office@kamsolutions.pl">
            <FontAwesomeIcon icon={faEnvelope} size="lg" style={{ color: '#26C17E',  marginRight: '20px' }}/>
            <span>office@kamsolutions.pl</span>
        </a>
    </li>
    <li>
        <a href="https://twitter.com/wordpress" target="_blank" rel="noopener noreferrer">
            <FontAwesomeIcon icon={faTwitter} size="lg" style={{ color: '#26C17E',  marginRight: '20px' }} />
            <span>Twitter</span>
            <p className='greyText'>https://twitter.com/wordpress</p>
        </a>
    </li>
    <li>
        <a href="https://www.facebook.com/facebook" target="_blank" rel="noopener noreferrer">
            <FontAwesomeIcon icon={faFacebookF} size="lg" style={{ color:'#26C17E',  marginRight: '20px'}} />
            <span>Facebook</span>
            <p className='greyText'>https://www.facebook.com/facebook</p>
        </a>
    </li>
    <li>
        <a href="skype:kamsolutions.pl?call">
            <FontAwesomeIcon icon={faSkype} size="lg" style={{ color:'#26C17E',  marginRight: '20px'}} />
            <span>Skype</span>
            <p className='greyText'>kamsolutions.pl </p>
        </a>
    </li>
</ul>
     </div>
    );
}

export default Address;