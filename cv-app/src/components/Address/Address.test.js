import React from 'react';
import { render, screen } from '@testing-library/react';
import Address from './Address';

describe('Address component', () => {
  it('renders Contacts title', () => {
    render(<Address />);
    const titleElement = screen.getByText(/Contacts/i);
    expect(titleElement).toBeInTheDocument();
  });

  it('renders phone number correctly', () => {
    render(<Address />);
    const phoneNumberElement = screen.getByText(/500 342 242/i);
    expect(phoneNumberElement).toBeInTheDocument();
  });

  it('renders email correctly', () => {
    render(<Address />);
    const emailElement = screen.getByText(/office@kamsolutions.pl/i);
    expect(emailElement).toBeInTheDocument();
  });

  it('renders twitter link correctly', () => {
    render(<Address />);
    const twitterLinkElement = screen.getByText(/https:\/\/twitter.com\/wordpress/i);
    expect(twitterLinkElement).toBeInTheDocument();
  });
});