import { Server, Response } from 'miragejs';
import server from './server'; 

describe('MirageJS Server', () => {
  let testServer;

  beforeEach(() => {
    testServer = new Server({ environment: 'test' });
    server();  
  });

  afterEach(() => {
    testServer.shutdown();
  });

  it('returns the correct data for /educations', async () => {
    const response = await fetch('/api/educations');
    const data = await response.json();

    expect(response.status).toBe(200);
    expect(data).toEqual([{ date: '2023-01-01', title: 'Bachelor of Computer Science', description: 'Description...' }]);
  });

  it('returns the correct data for /skills', async () => {
    const response = await fetch('/api/skills');
    const data = await response.json();

    expect(response.status).toBe(200);
    expect(data).toEqual([{ name: 'JavaScript', range: 8 }]);
  });

  it('correctly handles a POST request to /skills', async () => {
    const requestBody = { name: 'React', range: 9 };
    const response = await fetch('/api/skills', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(requestBody)
    });
    const data = await response.json();

    expect(response.status).toBe(200);
    expect(data).toEqual(requestBody);
  });
});