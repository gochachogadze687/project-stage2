import { createServer, Response } from 'miragejs';

export default function server() {
  createServer({
    routes() {
      this.namespace = '/api';

      this.timing = 3000; 

      this.get('/educations', () => {
        return new Response(200, {}, [{ date: '2023-01-01', title: 'Bachelor of Computer Science', description: 'Description...' }]);
      });

      this.get('/skills', () => {
        return new Response(200, {}, [{ name: 'JavaScript', range: 8 }]);
      });

      this.post('/skills', (schema, request) => {
        let attrs = JSON.parse(request.requestBody);
        
        return new Response(200, {}, attrs);
      });
    },
  });
}