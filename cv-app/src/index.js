import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import server from './services/server';
import { Provider } from 'react-redux';
import { store } from './app/store';

export function runApp() {
  server();

  const rootElement = document.getElementById('root');
  const root = ReactDOM.createRoot(rootElement);
  root.render(
    <React.StrictMode>
      <Provider store={store}>
        <App />
      </Provider>
    </React.StrictMode>
  );

  reportWebVitals();
}

export const shouldRunApp = process.env.NODE_ENV !== 'test';

if (shouldRunApp) {
  runApp();
}